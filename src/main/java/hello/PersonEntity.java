
package hello;

import java.util.*;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.data.annotation.Transient;

@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonEntity {

    @Id
    @GeneratedValue
    private Long generatedId;
    private String id;
    private String name;
    private PersonType type;
    private List<String> avestanName;
    private List<String> pahlaviName;
    private String englishName;
    @Relationship(type = "FATHER", direction = Relationship.INCOMING)
    private PersonEntity father;
	@Relationship(type = "MOTHER", direction = Relationship.INCOMING)
	private PersonEntity mother;
	@Relationship(type = "PREDECESSOR", direction = Relationship.INCOMING)
    private PersonEntity predecessor;
    @Relationship(type = "SPOUSE", direction = Relationship.UNDIRECTED)
    private PersonEntity spouse;


    private String dynasty;
    private Integer age;
    private String year;
    private String fatherId;
    private String motherId;
    private String spouseId;
    private String predecessorId;
    private GenderType gender;


    public PersonEntity() {
        // Empty constructor required as of Neo4j API 2.0.5
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonType getType() {
        return type;
    }

    public void setType(PersonType type) {
        this.type = type;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getDynasty() {
        return dynasty;
    }

    public void setDynasty(String dynasty) {
        this.dynasty = dynasty;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

	public PersonEntity getFather() {
		return father;
	}

	public void setFather(PersonEntity father) {
		this.father = father;
	}

	public PersonEntity getMother() {
		return mother;
	}

	public void setMother(PersonEntity mother) {
		this.mother = mother;
	}

	@Transient
	public String getFatherId() {
		return fatherId;
	}

	public void setFatherId(String fatherId) {
		this.fatherId = fatherId;
	}

	@Transient
	public String getMotherId() {
		return motherId;
	}



	public void setMotherId(String motherId) {
		this.motherId = motherId;
	}

    public PersonEntity getSpouse() {
        return spouse;
    }

    public void setSpouse(PersonEntity spouse) {
        this.spouse = spouse;
    }

    @Transient
    public String getSpouseId() {
        return spouseId;
    }

    public void setSpouseId(String spouseId) {
        this.spouseId = spouseId;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public Long getGeneratedId() {
        return generatedId;
    }

    public void setGeneratedId(Long generatedId) {
        this.generatedId = generatedId;
    }

    public PersonEntity getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(PersonEntity successor) {
        this.predecessor = successor;
    }

	@Transient
    public String getPredecessorId() {
        return predecessorId;
    }

    public void setPredecessorId(String predecessorId) {
        this.predecessorId = predecessorId;
    }

    public List<String> getAvestanName() {
        return avestanName;
    }

    public void setAvestanName(List<String> avestanName) {
        this.avestanName = avestanName;
    }

    public List<String> getPahlaviName() {
        return pahlaviName;
    }

    public void setPahlaviName(List<String> pahlaviName) {
        this.pahlaviName = pahlaviName;
    }
}


package hello;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@SpringBootApplication
@EnableNeo4jRepositories
public class Application implements ApplicationRunner {

    private final static Logger log = LoggerFactory.getLogger(Application.class);
    private Map<String, PersonEntity> personsMap = new HashMap<>();


    @Autowired
    private PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        String s = Files.lines(Paths.get("/home/pooya/projects/neo4j-spring/tutorials/gs-accessing-data-neo4j/complete/src/main/resources/shahname.json"))
                .reduce((i, j) -> i + j).get();
        ObjectMapper objectMapper = new ObjectMapper();
        Map entity = objectMapper.readValue(s, Map.class);
        List<?> kings = (List) entity.get("kings");

        kings.forEach(i -> {
            try {
                String value = objectMapper.writeValueAsString(i);
                PersonEntity readValue = objectMapper.readValue(value, PersonEntity.class);
				personsMap.put(readValue.getId(), readValue);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        personsMap.forEach((key, value) -> {
			value.setFather(personsMap.get(value.getFatherId()));
			value.setMother(personsMap.get(value.getMotherId()));
			value.setPredecessor(personsMap.get(value.getPredecessorId()));
			personRepository.save(value);
		});

		System.out.println("FINISHED!!!!!");

		System.exit(0);
    }
}
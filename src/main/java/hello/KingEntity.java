
package hello;

import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class KingEntity {

    @Id
    private Long id;
    private String name;
    private PersonType type;
    private String avestanName;
    private String englishName;
    @Relationship(type = "PARENT", direction = "INCOMING")
    private KingEntity parent;
    private String dynasty;
    private Integer age;
    private String year;


    public KingEntity() {
        // Empty constructor required as of Neo4j API 2.0.5
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersonType getType() {
        return type;
    }

    public void setType(PersonType type) {
        this.type = type;
    }

    public String getAvestanName() {
        return avestanName;
    }

    public void setAvestanName(String avestanName) {
        this.avestanName = avestanName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public KingEntity getParent() {
        return parent;
    }

    public void setParent(KingEntity parent) {
        this.parent = parent;
    }

    public String getDynasty() {
        return dynasty;
    }

    public void setDynasty(String dynasty) {
        this.dynasty = dynasty;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}